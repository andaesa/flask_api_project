from flask import Flask, jsonify
from flask_restful import *
from flask_sqlalchemy import SQLAlchemy
from models import *
import json

app = Flask(__name__)
api = Api(app)

class node(Resource):
        def get(self, id):
                result = Node_Data.query.get(id)
                r = (result.serialize())
                return jsonify(data = r)

class nodelist(Resource):
        def get(self):
                results = Node_Data.query.all()
                r = (Node_Data.serialize_list(results))
                return jsonify(data_list = r)

api.add_resource(nodelist, "/nodes")
api.add_resource(node, "/nodes/<id>")

if __name__ == '__main__':
        app.run(host='0.0.0.0')