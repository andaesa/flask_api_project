1. Run 'python models.py' to create the database
2. Run 'python mqttsub.py' to subscribe to a broker, receive the message and save it to database. Then run 'python mqttpub.py' to publish message to a broker
3. Run 'python api.py' to view the message taken from the database in web browser.
