import paho.mqtt.client as mqtt
from models import *
from flask_sqlalchemy import SQLAlchemy

def on_connect(client, userdata, rc):
        print("connected with result code" + str(rc))

        client.subscribe("randomdata1")

def on_message(client, userdata, msg):
        print "Topic:", msg.topic + " " + "Temperature:" + str(msg.payload) + " " + "Time:" + str(msg.timestamp)

        raw_data1 = Node_Data(Temperature=msg.payload, Time=msg.timestamp)

        db.session.add(raw_data1)
        db.session.commit()


client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("128.199.121.240",1883, 60)

client.loop_forever()



