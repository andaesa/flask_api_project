import paho.mqtt.client as mqtt
import schedule, time, random

client = mqtt.Client()
client.connect("128.199.121.240", 1883, 60)

def pub_message():
        tempreading = random.uniform(0, 100)
#       pHreading = random.uniform(1,14)
#       oxyreading = random.uniform(0, 100)

        client.publish("randomdata1", tempreading)


schedule.every(1).minutes.do(pub_message)

while True:
        schedule.run_pending()
        time.sleep(1)

client.disconnect()
