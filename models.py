from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.inspection import inspect

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://nasarudin:falcon@localhost/mydatabase'
db = SQLAlchemy(app)

class Serializer(object):

        def serialize(self):
                return {c: getattr(self, c) for c in inspect(self).attrs.keys()}

        @staticmethod
        def serialize_list(l):
                return [m.serialize() for m in l]

class Node_Data(db.Model, Serializer):
        __tablename__ = 'raw_data'
        id = db.Column(db.Integer, primary_key=True)
        Temperature = db.Column(db.Float)
        Time = db.Column(db.Float)

        def __init__(self, Temperature, Time):
                self.Temperature = Temperature
                self.Time = Time

        def json_dump(self):
                return dict(Temperature = self.Temperature, Time = self.Time)


        def serialize(self):
                d = Serializer.serialize(self)
                return d

db.create_all()
